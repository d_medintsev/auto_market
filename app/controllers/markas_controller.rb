class MarkasController < ApplicationController
  # GET /markas
  # GET /markas.json
  def index
    @markas = Marka.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @markas }
    end
  end

  # GET /markas/1
  # GET /markas/1.json
  def show
    @marka = Marka.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @marka }
    end
  end

  # GET /markas/new
  # GET /markas/new.json
  def new
    @marka = Marka.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @marka }
    end
  end

  # GET /markas/1/edit
  def edit
    @marka = Marka.find(params[:id])
  end

  # POST /markas
  # POST /markas.json
  def create
    @marka = Marka.new(params[:marka])

    respond_to do |format|
      if @marka.save
        format.html { redirect_to @marka, notice: 'Marka was successfully created.' }
        format.json { render json: @marka, status: :created, location: @marka }
      else
        format.html { render action: "new" }
        format.json { render json: @marka.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /markas/1
  # PUT /markas/1.json
  def update
    @marka = Marka.find(params[:id])

    respond_to do |format|
      if @marka.update_attributes(params[:marka])
        format.html { redirect_to @marka, notice: 'Marka was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @marka.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /markas/1
  # DELETE /markas/1.json
  def destroy
    @marka = Marka.find(params[:id])
    @marka.destroy

    respond_to do |format|
      format.html { redirect_to markas_url }
      format.json { head :ok }
    end
  end
end
