# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  email      :string(255)
#  first_name :string(255)
#  last_name  :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class User < ActiveRecord::Base
  # Validates
validates :first_name, presence: true, :format => { :with => /\A[a-zA-Z]+\z/, :message => "Only letters allowed" }
validates :last_name, :format => { :with => /\A[a-zA-Z]+\z/, :message => "Only letters allowed" }
validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

end
