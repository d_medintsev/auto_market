# == Schema Information
#
# Table name: cars
#
#  id         :integer          not null, primary key
#  created_at :datetime
#  updated_at :datetime
#  user_id    :integer
#  marka_id   :integer
#

class Car < ActiveRecord::Base
	# Validates
belongs_to :user
validates :user, presence: true
belongs_to :marka
validates :marka, presence: true
end

