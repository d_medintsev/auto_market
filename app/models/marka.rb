# == Schema Information
#
# Table name: markas
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  manufacturer_id :integer
#  year_start      :integer
#  year_end        :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class Marka < ActiveRecord::Base
	validates :manufacturer, presence: true
	belongs_to :manufacturer
  validates :year_start,  presence: true, :format => { :with => /\A[0-9]+\z/, :message => "Only digits allowed" }
	validates_numericality_of :year_end,  presence: true, :greater_than => :year_start, :on => :create	
end