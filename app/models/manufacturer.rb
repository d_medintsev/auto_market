# == Schema Information
#
# Table name: manufacturers
#
#  id          :integer          not null, primary key
#  created_at  :datetime
#  updated_at  :datetime
#  name        :string(255)
#  country     :string(255)
#  description :text
#

class Manufacturer < ActiveRecord::Base
	# Validates
  has_many :marka
  validates_associated :marka
  validates :name,  presence: true, :format => { :with => /\A[a-zA-Z]+\z/, :message => "Only letters allowed" }
  validates :country,  presence: true, :format => { :with => /\A[a-zA-Z]+\z/, :message => "Only letters allowed" }
  
end
