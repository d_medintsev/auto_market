require 'test_helper'

class MarkasControllerTest < ActionController::TestCase
  setup do
    @marka = markas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:markas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create marka" do
    assert_difference('Marka.count') do
      post :create, marka: @marka.attributes
    end

    assert_redirected_to marka_path(assigns(:marka))
  end

  test "should show marka" do
    get :show, id: @marka.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @marka.to_param
    assert_response :success
  end

  test "should update marka" do
    put :update, id: @marka.to_param, marka: @marka.attributes
    assert_redirected_to marka_path(assigns(:marka))
  end

  test "should destroy marka" do
    assert_difference('Marka.count', -1) do
      delete :destroy, id: @marka.to_param
    end

    assert_redirected_to markas_path
  end
end
