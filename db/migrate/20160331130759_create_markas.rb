class CreateMarkas < ActiveRecord::Migration
  def change
    create_table :markas do |t|
      t.string :name
      t.integer :manufacturer_id
      t.integer :year_start
      t.integer :year_end

      t.timestamps
    end
  end
end
